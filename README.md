# motorController

motorController is a platform for interfacing with a motor and encoder using an ATtiny microcontroller over I2C. This removes the computationally expensive hardware interrupts from the main project controller and adds digital I/O like motor speed and direction.

- [motorController](#motorcontroller)
  - [Features](#features)
    - [Encoder](#encoder)
    - [Motor](#motor)
    - [System](#system)
    - [Configuration](#configuration)
      - [Bit Positions](#bit-positions)
      - [Encoder Speed Prescaler](#encoder-speed-prescaler)
      - [Motor Speed and Direction](#motor-speed-and-direction)
  - [Wiring](#wiring)
    - [ATtiny412 Pinout](#attiny412-pinout)
    - [ATtiny1614 Pinout](#attiny1614-pinout)
    - [Example Setup](#example-setup)
  - [How To Make](#how-to-make)
    - [ATtiny1614](#attiny1614)
    - [ATtiny412](#attiny412)
  - [Links](#links)
    - [How to Make Your Own UPDI Programmer](#how-to-make-your-own-updi-programmer)
    - [Writing UPDI Fuse as GPIO Pin](#writing-updi-fuse-as-gpio-pin)

## Features
A motorController is capable of handling almost everything that is associated with controlling a motor. This includes reading in encoder ticks to determine encoder position and speed and outputting motor speed and direction. These features are common among all versions of the motorController. Some other features for the higher pin-count versions of the motorController include reading motor voltage and current and outputting motor power control which could be used for turning a motor on or off. 

See [Detailed Function Description](<documentation/Detailed Function Descriptions.md>) for more in-depth information on how to use each function. 

### Encoder
Encoder position is tracked using an hardware interrupt on the motorController. The motorController has been tested to support encoder rates from 0 Hz up to at least 200 kHz. Adjusting the encoder speed prescaler allows for tracking encoder speed in a variety of ranges which can be seen in the configuration section depending on the range that best fits the application. 

| Function Name            | ATtiny412 | ATtiny1614 | Function Description                                                                                                                                                     |
| :----------------------- | :-------: | :--------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| getEncoderPosition()     |    Yes    |    Yes     | This returns a 32-bit signed integer representing the current position of the encoder                                                                                    |
| getEncocerSpeed()        |    Yes    |    Yes     | This returns a floating point representation of the current speed of the encoder. The range of possible readings can be adjusted using the setSpeedPrescaler() function. |
| setEncoderPosition()     |    Yes    |    Yes     | This sets the encoder position to a specified number.                                                                                                                    |
| setEncoderPositionZero() |    Yes    |    Yes     | This sets the enccoder position to zero.                                                                                                                                 |
| setEncoderSpeedPrescaler()      |    Yes    |    Yes     | This adjusts the range of possible values of the encoder speed function. See the section on encoder speed prescaler for more information on possible values.                     |

### Motor

| Function Name                  | ATtiny412 | ATtiny1614 | Function Description                                                                                                                                                                                                         |
| :----------------------------- | :-------: | :--------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| setMotorSpeed()                |    Yes    |    Yes     | This outputs an 8-bit PWM signal on the motor speed pin at 72.5kHz. The setMotorSpeedOrientation() function can be used to invert the orientation of the motor speed. See the section on configuration for more information. |
| setMotorDirection()            |    Yes    |    Yes     | This outputs a logic level value for forward and reverse. The setMotorDirectionOrientation() function can be used to invert the orientation of the motor direction. See the section on configuration for more information.   |
| setMotorSpeedOrientation()     |    Yes    |    Yes     | This inverts the orientation of the motor speed. See the section on configuration for more information.                                                                                                                      |
| setMotorDirectionOrientation() |    Yes    |    Yes     | This inverts the orientation of the motor direction. See the section on configuration for more information.                                                                                                                  |
| getMotorVoltage()              |    No     |    Yes     | This returns the voltage across the motor in milliVolts.                                                                                                                                                                     |
| getMotorCurrent()              |    No     |    Yes     | This returns the current going through the motor in milliAmps.                                                                                                                                                               |
| getMotorPower()                |    No     |    Yes     | This returns the power sunk by the motor in milliWatts.                                                                                                                                                                      |
| setMotorPowerOutput()          |    No     |    Yes     | This outputs a logic level value for motor power on and off. See the section on configuration for more information.                                                                                                          |

### System
| Function Name      | ATtiny412 | ATtiny1614 | Function Description                                                                                              |
| :----------------- | :-------: | :--------: | :---------------------------------------------------------------------------------------------------------------- |
| getStatus()        |    Yes    |    Yes     | Returns the status of the I2C bus from the most recent communication with the device.                             |
| getSupplyVoltage() |    Yes    |    Yes     | Returns the supply voltage of the motorController device in milliVolts.                                           |
| getTemperature()   |    Yes    |    Yes     | Returns the temperature of the motorController device in Celsius.                                                 |
| setConfig()        |    Yes    |    Yes     | Sets the configuration of the motorController device. See the configuration section on how to select each option. |


### Configuration
The following diagram shows what each bit of the configuration does. Notice all of these bits have their own respective functions as well. You can either set the configuration all at once or individually using each of the functions provided. Setting the configuration all at once using setConfig() gives better performance and uses less time on the I2C bus. 

#### Bit Positions
```
0bXXXXXXXX
  |||||||+> Motor Direction
  ||||||+-> Motor Speed Orientation
  |||||+--> Motor Direction Orientation
  ||||+---> Motor Power Output
  |||+----> Speed Prescaler Bit 0
  ||+-----> Speed Prescaler Bit 1
  |+------> Speed Prescaler Bit 2
  +-------> N/A
```

#### Encoder Speed Prescaler
The encoder speed calculation is based on a 16-bit timer therefore it can only contain values from 0 to 65535. The following table shows the different options for scaling the system clock for the speed calculation timer. This allows for better customization of the encoder speed value by changing the range of possible values. If you need more granularity in lower speed values and don't need a lot of granularity in higher speeds, you can use a higher scaling value from the table. Note, there is not currently a good way to determine if the frequency being measured is outside the range of possible values given the current scaling value. 

| Bit 2 | Bit 1 | Bit 0 | Scaling Value       | Range Minimum |  Range Maximum |
| :---: | :---: | :---: | :------------------ | ------------: | -------------: |
|   0   |   0   |   0   | System Clock / 1    |     305.18 Hz | 20000000.00 Hz |
|   0   |   0   |   1   | System Clock / 2    |     152.59 Hz | 10000000.00 Hz |
|   0   |   1   |   0   | System Clock / 4    |      76.29 Hz |  5000000.00 Hz |
|   0   |   1   |   1   | System Clock / 8    |      38.15 Hz |  2500000.00 Hz |
|   1   |   0   |   0   | System Clock / 16   |      19.07 Hz |  1250000.00 Hz |
|   1   |   0   |   1   | System Clock / 64   |       4.77 Hz |   312500.00 Hz |
|   1   |   1   |   0   | System Clock / 256  |       1.19 Hz |    78125.00 Hz |
|   1   |   1   |   1   | System Clock / 1024 |       0.30 Hz |    19531.25 Hz |

#### Motor Speed and Direction
The following table shows how the configuration bits can be used to invert the motor speed and motor direction. This is useful on specific motors which require inverted PWM timing or inverted direction pins. 

When the motor direction orientation is a 0, the motor direction output is the same as the motor direction input (0 stays 0 and 1 stays 1). When the motor direction orientation is a 1, the motor direction output is inverted from the motor direction output (0 becomes 1 and 1 becomes 0). When the motor speed orientation is a 0, the motor speed output is the same as the motor speed input (30 becomes 30). When the motor speed orientation is a 1, the motor speed output is inverted from the motor speed input (30 becomes 225).

| Input Motor Direction Orientation | Input Motor Speed Orientation | Input Motor Direction | Input Motor Speed | Output Motor Direction | Output Motor Speed |
| :-------------------------------: | :---------------------------: | :-------------------: | :---------------: | :--------------------: | :----------------: |
|                 0                 |               0               |           0           |        50         |           0            |         50         |
|                 0                 |               0               |           1           |        50         |           1            |         50         |
|                 0                 |               1               |           0           |        50         |           0            |        205         |
|                 0                 |               1               |           1           |        50         |           1            |        205         |
|                 1                 |               0               |           0           |        50         |           1            |         50         |
|                 1                 |               0               |           1           |        50         |           0            |         50         |
|                 1                 |               1               |           0           |        50         |           1            |        205         |
|                 1                 |               1               |           1           |        50         |           0            |        205         |

## Wiring
Below are the pinouts for both the ATtiny412 and the ATtiny1614 versions of the motorController. The ATtiny412 should be used in smaller applications where size is key and information feedback isn't necessary. All other applications should probably use the ATtiny1614 because of it's additional features. 

### ATtiny412 Pinout
```
                      ____
                VCC -|    |- GND
    Motor Direction -|    |- Encoder 1
    Motor Speed PWM -|    |- Encoder 2 (UPDI)
            I2C SDA -|____|- I2C SCL
```

### ATtiny1614 Pinout
```
                      ____
                VCC -|    |- GND
                    -|    |- Encoder 1
    Motor Speed PWM -|    |- Encoder 2
    Motor Direction -|    |- Motor Current
    Motor Power Out -|    |- Motor Voltage (UPDI)
                    -|    |- I2C SCL
                    -|____|- I2C SDA
```

### Example Setup
Below is what a wiring diagram would look like when connecting two motorControllers (indicated by the mC 00 and mC 01) to a microcontroller (indicated by the uC). The ATtiny Series-1 line of microcontrollers have built-in pullup resistors on the I2C bus meaning no external pullup resistors are required. Make sure to setup the two motorControllers with unique I2C addresses before connecting them to the same I2C bus. 
```
                 VCC        GND
                  T          T
                  |   ____   |
                  +--|    |--+
                <----| mC |----> 
                <----| 00 |----> 
             +---SDA-|____|-SCL---+
             |                    |
             |   VCC        GND   |
             |    T          T    |
             |    |   ____   |    |
             |    +--|    |--+    |
             |  <----| mC |---->  |
             |  <----| 01 |---->  |
             +---SDA-|____|-SCL---+
             |                    |
             |   VCC        GND   |
             |    T          T    |
             |    |   ____   |    |
             |    +--|    |--+    |
             |  <----| uC |---->  |
             |  <----|    |---->  |
             +---SDA-|____|-SCL---+

```

## How To Make
If you want to make your own, you can purchase an ATtiny1614 and program it using the jtag2updi program on an ATmega328-based microcontroller (like the Arduino Uno or Arduino Nano). Making your own motorController based on the ATtiny412 has a few more steps required to get it up and working. See the links section for instructions on how to make your own jtag2updi programmer. 

### ATtiny1614
This can be done easily by following the steps provided by ElTangas to create your own jtag2updi programmer and flashing the motorController.ino file to the ATtiny1614.

### ATtiny412
This requries a little bit more effort but can be used in really small applications. To program an ATtiny412 with the motorController program, a high voltage programmer would be needed because the UPDI pin needs to be configured as a GPIO pin. Once the UPDI pin is configured as a GPIO pin, programming the ATtiny412 requires a high voltage programmer. In the near future I will hopefully be developing my own ATtiny-based High Voltage UPDI programmer, more details will follow. Configuring the UPDI pin as a GPIO pin is a semi-permanent operation in that it is done by writing a fuse to the microcontroller which can only be changed by writing over the same fuse again. This is not something normally done when flashing the microcontroller and requires a few extra steps. See the section on writing the UPDI fuse as a GPIO pin

## Links
### How to Make Your Own UPDI Programmer
* ElTangas/jtag2updi (Original UPDI programmer) -  https://github.com/ElTangas/jtag2updi
* Dlloydev/jtag2updi (High Voltage UPDI programmer) -  https://github.com/Dlloydev/jtag2updi
  
### Writing UPDI Fuse as GPIO Pin
* Configuring the UPDI pin as a GPIO pin (required for motorController ATtiny412) - https://github.com/Dlloydev/jtag2updi/wiki/UPDI-pin-fused-as-Reset-or-GPIO-(non-optiboot)
