//*******************************************************//
// Nathaniel Belles
// motorController
// To run on microcontroller to interface with motors
//*******************************************************//

// Include libraries
#include <Arduino.h>
#include <Wire.h>
#include "motor.h"

motor motor1(B00000001);

// Setup function to run once
void setup() {
    // Setup serial port
    Serial.begin(2000000);

    motor1.setEncoderPosition(123456);
    motor1.setMotorSpeed(123);
}

// Loop function to run continuously
void loop() {
  for(int config = 0; config < 8; config++) {
    for(int speed = 0; speed < 256; speed++) {
      Serial.print("Config: "); Serial.println(config, BIN);
      Serial.print("MotorSpeed: "); Serial.println(speed);
      Serial.print("EncoderPosition: "); Serial.println(motor1.getEncoderPosition());
      Serial.print("EncoderSpeed: "); Serial.println(motor1.getEncoderSpeed());
      // Serial.print("SupplyVoltage: "); Serial.println(motor1.getSupplyVoltage());
      // Serial.print("Temperature: "); Serial.println(motor1.getTemperature()); // Still working on this.
      motor1.setConfig(config);
      motor1.setMotorSpeed(speed);
      delay(100);
    }
  }
}