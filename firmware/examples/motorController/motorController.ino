//*******************************************************//
// Nathaniel Belles
// motorController - motorController.ino
// To run on ATtiny 412 or 1614 as interface to motor
//*******************************************************//

// Include libraries
#include <Arduino.h>
#include <Wire.h>
#include "motorController.h"

// Setup function to run once
void setup() {
}

// Loop function to run continuously
void loop() {
    motorController *motorControllerInstance = motorController::getInstance();
    motorControllerInstance->update();
}