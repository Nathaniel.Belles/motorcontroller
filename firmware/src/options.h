// ********************************************************************** //
// Nathaniel Belles
// driveBase - options.h
// Purpose: Use for setup to change any options of the program
// ********************************************************************** //

#ifndef options_h
#define options_h

// ATtinyxy2 Pinout:
//                        ____
//                  VCC -|    |- GND
//            Encoder 1 -|    |- Encoder 2
//      Motor Speed PWM -|    |- Motor Direction (UPDI)
//              I2C SDA -|____|- I2C SCL
//

// ATtinyxy4 Pinout:
//                        ____
//                  VCC -|    |- GND
//                      -|    |- Encoder 1
//      Motor Speed PWM -|    |- Encoder 2
//      Motor Direction -|    |- Motor Current
//      Motor Power Out -|    |- Motor Voltage (UPDI)
//                      -|    |- I2C SCL
//                      -|____|- I2C SDA
//

// Choose the type of encoder to use:
    #define ENCODER_TYPE_SINGLE
    //#define ENCODER_TYPE_QUADRATURE

// Choose the I2C address of the controller
    #define I2C_CONTROLLER_ADDRESS B0000001

// Choose the orientation of motor speed
    //#define MOTOR_SPEED_NORMAL                // PWM 0 = off, 255 = fastest
    #define MOTOR_SPEED_INVERTED                // PWM 0 = fastest, 255 = off

// Choose the orientation of motor direction
    #define MOTOR_DIRECTION_NORMAL              // Forward normal
    //#define MOTOR_DIRECTION_INVERTED          // Reverse normal

// Check defines
    // Encoder type
        #ifdef ENCODER_TYPE_QUADRATURE
            #define ENCODER_TYPE 0
        #endif
        #ifdef ENCODER_TYPE_SINGLE
            #define ENCODER_TYPE 1
        #endif
        #ifndef ENCODER_TYPE
            #error Please choose type of encoder by adding "#define ENCODER_TYPE_QUADRATURE" or "#define ENCODER_TYPE_SINGLE" to the options.h file!
        #endif

    // Motor speed orientation
        #ifdef MOTOR_SPEED_NORMAL
            #define MOTOR_SPEED_ORIENTATION 0
        #endif
        #ifdef MOTOR_SPEED_INVERTED
            #define MOTOR_SPEED_ORIENTATION 1
        #endif
        #ifndef MOTOR_SPEED_ORIENTATION
            #error Please choose motor speed orientation by adding "#define MOTOR_SPEED_NORMAL" or "#define MOTOR_SPEED_INVERTED" to the options.h file!
        #endif

    // Motor direction
        #ifdef MOTOR_DIRECTION_NORMAL
            #define MOTOR_DIRECTION_ORIENTATION 0
        #endif
        #ifdef MOTOR_DIRECTION_INVERTED
            #define MOTOR_DIRECTION_ORIENTATION 1
        #endif
        #ifndef MOTOR_DIRECTION_ORIENTATION
            #error Please choose motor direction by adding "#define MOTOR_DIRECTION_NORMAL" or "#define MOTOR_DIRECTION_INVERTED" to the options.h file!
        #endif

    // I2C address
        #ifndef I2C_CONTROLLER_ADDRESS
            #error Please choose I2C controller address by adding "#define I2C_CONTROLLER_ADDRESS" to the options.h file!
        #endif

// Digital pins
    // ATtiny 412
    #ifdef __AVR_ATtinyxy2__
        #define DIGITAL_PIN_TRIGGER_1 0
        #define DIGITAL_PIN_TRIGGER_2 4
        #define DIGITAL_PIN_I2C_SDA 2
        #define DIGITAL_PIN_ISC_SCL 3
        #define DIGITAL_PIN_MOTOR_SPEED 1
        #define DIGITAL_PIN_MOTOR_DIRECTION 5
    #endif

    // ATtiny 1614
    #ifdef __AVR_ATtinyxy4__
        #define DIGITAL_PIN_TRIGGER_1 2
        #define DIGITAL_PIN_TRIGGER_2 3
        #define DIGITAL_PIN_I2C_SDA 6
        #define DIGITAL_PIN_ISC_SCL 7
        #define DIGITAL_PIN_MOTOR_SPEED 10
        #define DIGITAL_PIN_MOTOR_DIRECTION 9
        #define DIGITAL_PIN_MOTOR_POWER 0
        #define ANALOG_PIN_MOTOR_VOLTAGE_INPUT 11
        #define ANALOG_PIN_MOTOR_CURRENT_INPUT 8

        #define MOTOR_VOLTAGE_UPPERBOUND 0 // 0 Volts
        #define MOTOR_VOLTAGE_LOWERBOUND 12000 // 12 Volts
        #define MOTOR_CURRENT_UPPERBOUND 10000 // 10 Amps
        #define MOTOR_CURRENT_LOWERBOUND -10000 // -10 Amps
    #endif

// EEPROM Memory Locations
#define EEPROM_I2C_ADDRESS 0
#define EEPROM_CONFIG 1

#define RETURN_ERROR 0

#endif