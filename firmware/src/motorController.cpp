// ********************************************************************** //
// Nathaniel Belles
// driveBase - motorController.cpp
// Purpose: Creates object of type 'motorController' to use to interface 
//          with a motor instance in a driveBase object
// ********************************************************************** //

// Include necessary libraries
#include <Arduino.h>
#include <Wire.h>
#include <EEPROM.h>
#include "motorConverters.h"
#include "motorCommands.h"
#include "motorController.h"
// #include "iotn412.h"
// #include "interrupt.h"

// Converters for I2C
S32toU8 converterS32toU8;
U32toU8 converterU32toU8;
U16toU8 converterU16toU8;

// Variable for ISR's
volatile int32_t encoderPosition = 0;
volatile uint16_t encoderSpeed = 0;

// Variables for motor controller instance
bool motorController::_instanceCreated = false;                             // Stores instance created
motorController* motorController::_instance = NULL;                         // Stores pointer to instance
uint8_t motorController::_address = 0;                                      // Stores address for I2C

uint16_t motorController::_supplyVoltage = 0;                               // Stores the supply voltage (in milliVolts)
uint16_t motorController::_temperature = 0;                                 // Stores tempterature of die
motorCommand motorController::_motorCommand = DefaultValue;                 // Stores which variable to be sent
uint8_t motorController::_motorSpeed = 0;                                   // Stores the output motor speed
bool motorController::_motorDirection = 0;                                  // Stores the motor direction
bool motorController::_motorSpeedOrientation = 0;                           // Stores the motor speed orientation
bool motorController::_motorDirectionOrientation = 0;                       // Stores the motor direction orientation
bool motorController::_motorPowerOutput = 0;                                // Stores the motor power output
uint8_t motorController::_config = B00000000;                               // Stores the motor configuration

//******************************************************************************//
// Constructors
//******************************************************************************//

// Singleton instance
motorController* motorController::getInstance() {
  if (!motorController::_instanceCreated) {
    motorController::_instance = new motorController();
    motorController::_instanceCreated = true;
  }
  return motorController::_instance;
}

// Default constructor
motorController::motorController() {
    // Setup I2C with address
    if(EEPROM.read(EEPROM_I2C_ADDRESS) != B11111111) {
        _setAddress(EEPROM.read(EEPROM_I2C_ADDRESS));
    } else {
        _setAddress(B00000001);
    }

    // Initialize position
    motorController::_setEncoderPositionZero();

    // Setup TCA0 and TCB0 timers and Event System for speed calculation
    EVSYS.ASYNCCH0 = 0x0D;                                                  // Set ASYNCCH0 Generator to PORTA_PIN6: 0x10 PORTA_PIN1: 0x0B
    EVSYS.ASYNCUSER0 = 0x03;                                                // Set TCB User Input to ASYNCCH0
    TCA0.SPLIT.CTRLA = 0x00;                                                // Disable TCA0 and set prescaler to 1
    TCA0.SPLIT.CTRLESET = TCA_SPLIT_CMD_RESET_gc | 0x03;                    // Set CMD to RESET, and enable on both pins.
    TCA0.SPLIT.CTRLA |= 0x01;                                               // Set TCA0's clock prescaler (0x01 to 0x0F)
    TCB0.CTRLA = 0x04;                                                      // Set Clock Selection
    TCB0.CTRLB = 0x43;                                                      // Set ASYNC, CCMPEN, and Count Mode to Capture Frequency Measurement Mode
    TCB0.EVCTRL = 0x01;                                                     // Set Capture Event Input Enable
    TCB0.CTRLA |= 0x01;                                                     // Set Timer to Enable

    // Setup TCD0 for PWM ouput
    TCD0.CTRLA=0x60;                                                        // Stop the timer and set clock to system clock
    while(!(TCD0.STATUS & 0x01)) {;}                                        // Wait until actually stopped
    TCD0.CTRLB |= TCD_WGMODE_ONERAMP_gc;                                    // Set waveform generation mode
    TCD0.CMPASET = 127;                                                     // Waveform high
    TCD0.CMPACLR = 255;                                                     // Waveform low
    TCD0.CMPBSET = 127;                                                     // Waveform high
    TCD0.CMPBCLR = 255;                                                     // Waveform low
    _PROTECTED_WRITE(TCD0.FAULTCTRL, TCD_CMPBEN_bm | TCD_CMPB_bm);          // Enable comparison B
    while (!(TCD0.STATUS & TCD_ENRDY_bm)) {;}                               // Wait for it to be synchronized
    TCD0.CTRLA |= 0x01;                                                     // Start timer

    // Set motor config if EEPROM has been written to
    _setConfig(EEPROM.read(EEPROM_CONFIG));

    // Set I/O for txy2 parts
    #ifdef __AVR_ATtinyxy2__
        // Set pins as outputs
        PORTA.DIRSET = B11000000;                                           // Set PA6-7 as output
        PORTA.OUTCLR = B11000000;                                           // Set PA6-7 output low

        // Set pins as inputs
        PORTA.DIRCLR = B00001001;                                           // Set PA0 and PA3 as input
        #ifdef ENCODER_TYPE_SINGLE
            // Set pins as interrupts
            PORTA.PIN3CTRL = B00001010;                                     // Set PA3 as interrupt on rising edge with pullup
            PORTA.PIN0CTRL = B00001000;                                     // Set PA0 as pullup
        #endif
        #ifdef ENCODER_TYPE_QUADRATURE
            // Set pins as interrupts
            PORTA.PIN3CTRL = B00001010;                                     // Set PA3 as interrupt on rising edge with pullup
            PORTA.PIN0CTRL = B00001010;                                     // Set PA0 as interrupt on rising edge with pullup
        #endif
    #endif

    // Set I/O for txy4 parts
    #ifdef __AVR_ATtinyxy4__
        // Set pins as outputs
        PORTA.DIRSET = B11100000;                                           // Set PA5-7 as output
        PORTA.OUTCLR = B11100000;                                           // Set PA5-7 output low

        // Set pins as inputs
        PORTA.DIRCLR = B00001111;                                           // Set PA0-3 as input
        #ifdef ENCODER_TYPE_SINGLE
            // Set pins as interrupts
            PORTA.PIN3CTRL |= B00001010;                                    // Set PA3 as interrupt on rising edge with pullup
            PORTA.PIN2CTRL |= B00001000;                                    // Set PA2 as pullup
        #endif
        #ifdef ENCODER_TYPE_QUADRATURE
            // Set pins as interrupts
            PORTA.PIN3CTRL |= B00001010;                                    // Set PA3 as interrupt on rising edge with pullup
            PORTA.PIN2CTRL |= B00001010;                                    // Set PA2 as interrupt on rising edge with pullup
        #endif
    #endif

    // Setup I2C events
    Wire.onRequest(motorController::_requestEvent);                         // Setup I2C on request event
    Wire.onReceive(motorController::_receiveEvent);                         // Setup I2C on receive event
}

//******************************************************************************//
// Public Functions
//******************************************************************************//

// Do background calculations
void motorController::update() {

}

//******************************************************************************//
// Private Getters
//******************************************************************************//

// Returns address
uint8_t motorController::_getAddress() {
    // Return address
    return _address;
}

// Returns encoder position
int32_t motorController::_getEncoderPosition() {
    // Return encoder position
    return encoderPosition;
}

// Returns encoder speed
uint16_t motorController::_getEncoderSpeed() {
    // Store speed
    encoderSpeed = TCB0.CCMP;

    // Reset speed for next value
    TCB0.CCMP = 0;

    // Return encoder speed
    return encoderSpeed;
}

// Returns supply voltage
uint16_t motorController::_getSupplyVoltage() {
    // Setup voltage references for ADC
    analogReference(VDD);                                   // Set the analog reference to internal reference
    VREF.CTRLA = VREF_ADC0REFSEL_1V5_gc;                      // Set the ADC reference to the 1.5v reference
  
    // Read supply voltage
    _setSupplyVoltage(1534500 / analogRead(ADC_INTREF));

    // Return supply voltage
    return _supplyVoltage;
}

// Returns temperature
uint16_t motorController::_getTemperature() {
    // Get temperature calibration data from fuses
    int8_t sigrow_offset = SIGROW.TEMPSENSE1; // Read signed value from signature row
    uint8_t sigrow_gain = SIGROW.TEMPSENSE0; // Read unsigned value from signature row

    // Setup voltage references for ADC temporarily
    analogReference(INTERNAL1V1);
    ADC0.SAMPCTRL = 0x1F; // Appears very necessary!
    ADC0.CTRLD |= ADC_INITDLY_DLY32_gc; // Doesn't seem so necessary?

    // Store reading from ADC
    uint16_t adc_reading = analogRead(ADC_TEMPERATURE); // ADC conversion result with 1.1 V internal reference

    // Set voltage references for ADC back to defaults
    analogReference(VDD);
    ADC0.SAMPCTRL = 0x0;
    ADC0.CTRLD &= ~(ADC_INITDLY_gm);

    // Adjust reading to final value
    uint32_t temperature = adc_reading - sigrow_offset; // Adjust reading from calibration values
    temperature *= sigrow_gain; // Adjust reading from calibration values
    temperature += 0x80; // Add 1/2 to get correct rounding on division below
    temperature >>= 8; // Divide result to get Kelvin
    _temperature = temperature - 272;

    // Return supply voltage
    return _temperature;
}

// Returns current command
motorCommand motorController::_getMotorCommand() {
    // Return current command
    return _motorCommand;
}

// Return current motor speed
uint8_t motorController::_getMotorSpeed() {
    // Return motor speed
    return _motorSpeed;
}

// Return current motor direction
bool motorController::_getMotorDirection() {
    // Return motor speed
    return _motorDirection;
}

// Get motor speed orientation
bool motorController::_getMotorSpeedOrientation() {
    // Return motor speed orientation
    return _motorSpeedOrientation;
}

// Get motor direction orientation
bool motorController::_getMotorDirectionOrientation() {
    // Return motor direction orientation
    return _motorDirectionOrientation;
}

// Get motor voltage
uint16_t motorController::_getMotorVoltage() {
    #ifdef __AVR_ATtinyxy4__
        // Store reading from ADC (0 - 1024)
        uint16_t reading = analogRead(ANALOG_PIN_MOTOR_VOLTAGE_INPUT);

        // Map values from ADC to voltage in milliVolts
        reading = map(reading, 0, 1024, MOTOR_VOLTAGE_LOWERBOUND, MOTOR_VOLTAGE_UPPERBOUND);

        // Return motor voltage (in milliVolts)
        return reading;
    #else
        return RETURN_ERROR;
    #endif
}

// Get motor current
uint16_t motorController::_getMotorCurrent() {
    #ifdef __AVR_ATtinyxy4__
        // Store reading from ADC (0 - 1024)
        uint16_t reading = analogRead(ANALOG_PIN_MOTOR_CURRENT_INPUT);

        // Map values from ADC to current in milliAmps
        reading = map(reading, 0, 1024, MOTOR_CURRENT_LOWERBOUND, MOTOR_CURRENT_UPPERBOUND);
        
        // Return motor voltage (in milliAmps)
        return reading;
    #else
        return RETURN_ERROR;
    #endif
}

// Get motor power
uint16_t motorController::_getMotorPower() {
    #ifdef __AVR_ATtinyxy4__
        // Return motor voltage (in milliWatts)
        return (_getMotorVoltage() * _getMotorCurrent());
    #else
        return RETURN_ERROR;
    #endif
}

// Get motor power output
bool motorController::_getMotorPowerOutput() {
    #ifdef __AVR_ATtinyxy4__
        // Return motor power output
        return _motorPowerOutput;
    #else
        return RETURN_ERROR;
    #endif
}

// Get motor direction orientation
bool motorController::_getConfig() {
    // Return motor direction orientation
    return _config;
}

//******************************************************************************//
// Private Setters
//******************************************************************************//

// Set address
void motorController::_setAddress(uint8_t desiredAddress) {
    // Store address
    _address = desiredAddress;

    // Stop wire
    Wire.end();

    // Start I2C with new address
    Wire.begin(_address);
}

// Set encoder position
void motorController::_setEncoderPosition(int32_t desiredEncoderPosition) {
    // Store encoder position to ISR variable
    encoderPosition = desiredEncoderPosition;
}

// Set encoder position zero
void motorController::_setEncoderPositionZero() {
    // Store encoder position as zero for ISR variable
    encoderPosition = 0;
}

// Function to set the prescaler
void motorController::_setEncoderSpeedPrescaler(uint8_t desiredEncoderSpeedPrescaler) {
    // Stop the timer
    TCA0.SPLIT.CTRLA &= B11111110;

    // Change prescaler
    TCA0.SPLIT.CTRLA = (desiredEncoderSpeedPrescaler << 1);

    // Start the timer again
    TCA0.SPLIT.CTRLA |= B00000001;
}

// Set supply voltage
void motorController::_setSupplyVoltage(uint16_t desiredSupplyVoltage) {
    // Store supply voltage
    _supplyVoltage = desiredSupplyVoltage;
}

// Set motor command
void motorController::_setMotorCommand(motorCommand desiredMotorCommand) {
    // Store motor command
    _motorCommand = desiredMotorCommand;
}

// Set motor speed
void motorController::_setMotorSpeed(uint8_t desiredMotorSpeed) {
    // Check motor speed orientation
    if (_getMotorSpeedOrientation()) {
        // Store motor speed
        _motorSpeed = 255 - desiredMotorSpeed;
    }
    else {
        // Store motor speed
        _motorSpeed = desiredMotorSpeed;
    }

    // Check if all the way high or low
    if(_motorSpeed > 254){
        // Drive output high
        
    } else if (_motorSpeed < 1) {
        // Drive output low

    } else {
        // Update cycle timings from motor speed
        TCD0.CMPBSET = (255 - _motorSpeed);

        // Resynchronize to TCD0
        while(!(TCD0.STATUS&0x02)) {;} // Wait for any previous synchronization to be completed
        TCD0.CTRLE=0x02; //Synchronize
    }
}

// Set motor direction
void motorController::_setMotorDirection(bool desiredMotorDirection) {
    // Check motor direction orientation
    if (_getMotorDirectionOrientation()) {
        // Store motor direction
        _motorDirection = ! desiredMotorDirection;
    }
    else {
        // Store motor direction
        _motorDirection = desiredMotorDirection;
    }
}

// Set motor speed orientation
void motorController::_setMotorSpeedOrientation(bool desiredMotorSpeedOrientation) {
    // Return motor direction orientation
    _motorSpeedOrientation = desiredMotorSpeedOrientation;
}

// Set motor direction orientation
void motorController::_setMotorDirectionOrientation(bool desiredMotorDirectionOrientation) {
    // Return motor direction orientation
    _motorDirectionOrientation = desiredMotorDirectionOrientation;
}

// Set motor power output
void motorController::_setMotorPowerOutput(bool desiredMotorPowerOutput) {
    #ifdef __AVR_ATtinyxy4__
    // Return motor direction orientation
    _motorPowerOutput = desiredMotorPowerOutput;

    // Set motor direction again with new config
    _getMotorPowerOutput() ? PORTA.OUTSET = B10000000 : PORTA.OUTCLR = B10000000;
    #endif
}

// Function to setup motor configuration
void motorController::_setConfig(uint8_t desiredConfig) {
    // Store configuration
    _config = desiredConfig;

    // Update speed prescaler
    _setEncoderSpeedPrescaler((desiredConfig & B01110000) >> 4);

    #ifdef __AVR_ATtinyxy4__
    // Update motor power output
    (desiredConfig & B00001000) ? _setMotorPowerOutput(1) : _setMotorPowerOutput(0);
    #endif

    // Update motor direction orientation
    (desiredConfig & B00000100) ? _setMotorDirectionOrientation(1) : _setMotorDirectionOrientation(0);
    
    // Update motor speed orientation
    (desiredConfig & B00000010) ? _setMotorSpeedOrientation(1) : _setMotorSpeedOrientation(0);

    // Update motor direction
    (desiredConfig & B00000001) ? _setMotorDirection(1) : _setMotorDirection(0);

    // Set motor direction output
    _getMotorDirection() ? PORTA.OUTSET = PIN6_bm : PORTA.OUTCLR = PIN6_bm;
    
    // Set motor speed output
    if(_getMotorSpeed() > 254){
        // Drive output high
        
    } else if (_getMotorSpeed() < 1) {
        // Drive output low

    } else {
        // Update cycle timings from motor speed
        TCD0.CMPBSET = (255 - _getMotorSpeed());

        // Resynchronize to TCD0
        while(!(TCD0.STATUS&0x02)) {;} // Wait for any previous synchronization to be completed
        TCD0.CTRLE=0x02; //Synchronize
    }

    // Write config to EEPROM
    EEPROM.update(EEPROM_CONFIG, _getConfig());
}

//******************************************************************************//
// Private Functions
//******************************************************************************//

// Function to send variable to Wire bus
void motorController::_sendVariableUnsignedTwoBytes(uint16_t variableToSend) {
    // Create temporary byte
    uint8_t byteToSend = B00000000;

    // Store variable in temporary converter
    converterU16toU8.conversionU16 = variableToSend;

    for (int i = 0; i < 2; i++) {
        // Determine byte to send by parsing union
        byteToSend = converterU16toU8.conversionU8[i];

        // Send current byte
        Wire.write(byteToSend);
    }
}

// Function to send variable to Wire bus
void motorController::_sendVariableSignedFourBytes(int32_t variableToSend) {
    // Create temporary byte
    uint8_t byteToSend = B00000000;

    // Store variable in temporary converter
    converterS32toU8.conversionS32 = variableToSend;

    for (int i = 0; i < 4; i++) {
        // Determine byte to send by parsing union
        byteToSend = converterS32toU8.conversionU8[i];

        // Send current byte
        Wire.write(byteToSend);
    }
}

// Function to send variable to Wire bus
void motorController::_sendVariableUnsignedFourBytes(uint32_t variableToSend) {
    // Create temporary byte
    uint8_t byteToSend = B00000000;

    // Store variable in temporary converter
    converterU32toU8.conversionU32 = variableToSend;

    for (int i = 0; i < 4; i++) {
        // Determine byte to send by parsing union
        byteToSend = converterU32toU8.conversionU8[i];

        // Send current byte
        Wire.write(byteToSend);
    }
}

// Receive unsigned 1 byte variable
uint8_t motorController::_receiveVariableUnsignedOneByte() {
    // Create temporary byte
    uint8_t byteToReceive = Wire.read();

    // Return variable
    return byteToReceive;
}

// Receive signed 4 byte variable
int32_t motorController::_receiveVariableSignedFourBytes() {
    // Create temporary byte
    uint8_t byteToReceive = B00000000;

    for (int i = 0; i < 4; i++) {
        // Receive current byte
        byteToReceive = Wire.read();

        //Determine byte to receive
        converterS32toU8.conversionU8[i] = byteToReceive;
    }

    // Return converted variable
    return converterS32toU8.conversionS32;
}

// Function for I2C on request event
void motorController::_requestEvent() {
    // Switch between requested variables
    switch (_getMotorCommand()) {
        case DefaultValue : 
            // Nothing received yet
            break;
        case GetEncoderPosition : 
            // Send variable position
            _sendVariableSignedFourBytes(_getEncoderPosition());
            break;
        case GetEncoderSpeed :
            // Send variable speed
            _sendVariableUnsignedTwoBytes(_getEncoderSpeed());
            break;
        case GetMotorVoltage :
            // Send variable motor voltage (milliVolts)
            _sendVariableUnsignedTwoBytes(_getMotorVoltage());
        case GetMotorCurrent :
            // Send variable motor current (milliAmps)
            _sendVariableUnsignedTwoBytes(_getMotorCurrent());
        case GetMotorPower :
            // Send variable motor power (milliWatts)
            _sendVariableUnsignedTwoBytes(_getMotorPower());
        case GetSupplyVoltage :
            // Send variable supplyVoltage
            _sendVariableUnsignedTwoBytes(_getSupplyVoltage());
            break;
        case GetTemperature :
            // Send variable temperature
            _sendVariableUnsignedTwoBytes(_getTemperature());
        default : 
            // Otherwise do nothing
            break;
    }
}

// Function for I2C on receive event
void motorController::_receiveEvent(int numberBytes) {
    // Get command from the bus
    _setMotorCommand((motorCommand)Wire.read());  // Store the requested variable

    // Switch between requested variables
    switch (_getMotorCommand()) {
        case SetEncoderPosition : 
            // Set encoder position 
            _setEncoderPosition(_receiveVariableSignedFourBytes());
            break;
        case SetEncoderPositionZero :
            // Set position to zero
            _setEncoderPositionZero();
            break;
        case SetMotorSpeed :
            // Set motor speed
            _setMotorSpeed(_receiveVariableUnsignedOneByte());
            break;
        case SetConfig :
            // Set motor config
            _setConfig(_receiveVariableUnsignedOneByte());
            break;
        default :
            // Otherwise do nothing
            break;
    }
}

//******************************************************************************//
// ISR's
//******************************************************************************//

#ifdef ENCODER_TYPE_QUADRATURE
// Interrupt handler
ISR(PORTA_PORT_vect) {
    // Store current flags
    uint8_t flags = PORTA.INTFLAGS;

    // Clear flags
    PORTA.INTFLAGS = flags;

    // Triggered by pin 2 (D0) (PA6)
    if (flags & B01000000) {
        // Determine direction from pin 3 (D1) (PA7)
        if(PORTA.IN & B00001000) {
            // Decrement position
            encoderPosition--;
        } 
        else {
            // Increment position
            encoderPosition++;
        }
    }
    // Triggered by pin 3 (D1) (PA7)
    if (flags & B00001000) {
        // Determine direction from pin 2 (D0) (PA6)
        if(PORTA.IN & B01000000) {
            // Increment position
            encoderPosition++;
        }
        else {
            // Decrement position
            encoderPosition--;
        }
    }
}
#endif

#ifdef ENCODER_TYPE_SINGLE
// Interrupt handler
ISR(PORTA_PORT_vect) {
    // Store current flags
    uint8_t flags = PORTA.INTFLAGS;

    // Clear flags
    PORTA.INTFLAGS = flags;

    // Triggered by pin 2 (D0) (PA6)
    if (flags & B00001000) {
        // Determine direction from pin 3 (D1) (PA7)
        if(PORTA.IN & B00000001) {
            // Decrement position
            encoderPosition--;
        } 
        else {
            // Increment position
            encoderPosition++;
        }
    }
}
#endif