// ********************************************************************** //
// Nathaniel Belles
// driveBase - encoderConverters.h
// Purpose: Creates unions that can be used to get individual bytes from
//          multi-byte data types or variables. 
// ********************************************************************** //

#ifndef encoderConverters_h
#define encoderConverters_h

// Initializing 32toU8 union type for I2C conversion
union S32toU8 {
    // Uint8_t buffer array
    uint8_t conversionU8[4];

    // Uint32_t buffer
    int32_t conversionS32;
};

// Initializing U32toU8 union type for I2C conversion
union U32toU8 {
    // Uint8_t buffer array
    uint8_t conversionU8[4];

    // Uint32_t buffer
    uint32_t conversionU32;
};

// Initializing U16toU8 union type for I2C conversion
union U16toU8 {
    // Uint8_t buffer array
    uint8_t conversionU8[2];

    // Uint32_t buffer
    uint16_t conversionU16;
};

#endif