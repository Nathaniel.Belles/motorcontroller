// ********************************************************************** //
// Nathaniel Belles
// driveBase - motor.h
// Purpose: Creates an object of type 'motor' to use to interface with
//          a single motor controller. Handles all I2C communication 
//          required. 
// ********************************************************************** //

#ifndef encoder_h
#define encoder_h

#include <Arduino.h>
#include "motorCommands.h"

class motor {
    public:
        // Constructors
        motor(uint8_t address);

        // Getters
        int32_t getEncoderPosition();
        float getEncoderSpeed();
        uint16_t getMotorVoltage();
        uint16_t getMotorCurrent();
        uint16_t getMotorPower();
        uint16_t getSupplyVoltage();
        uint16_t getTemperature();
        uint8_t getStatus();

        // Setters
        void setEncoderPosition(int32_t desiredEncoderPosition);
        void setEncoderPositionZero();
        void setEncoderSpeedPrescaler(uint8_t desiredEncoderSpeedPrescaler);
        void setMotorSpeed(uint8_t desiredMotorSpeed);
        void setMotorDirection(bool desiredMotorDirection);
        void setMotorSpeedOrientation(bool desiredMotorSpeedOrientation);
        void setMotorDirectionOrientation(bool desiredMotorDirectionOrientation); 
        void setMotorPowerOutput(bool desiredMotorPowerOutput); 
        void setConfig(uint8_t desiredConfig);

    private:
        // Variables
        uint8_t _address;
        uint8_t _config;
        uint8_t _status;

        // Getters
        uint8_t _getConfig();

        // Setters
        void _setConfig(uint8_t desiredConfig);

        // Functions
        void _sendMotorCommand(motorCommand commandToSend);
        uint8_t _getUnsignedOneByte();
        uint16_t _getUnsignedTwoBytes();
        int32_t _getSignedFourBytes();
        uint32_t _getUnsignedFourBytes();
        void _putUnsignedOneByte(uint8_t variableToSend);
        void _putSignedFourBytes(int32_t variableToSend);
        void _clearWire();
};

#endif