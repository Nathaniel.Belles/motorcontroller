// ********************************************************************** //
// Nathaniel Belles
// driveBase - motorController.h
// Purpose: Creates object of type 'motorController' to use to interface 
//          with an motor instance in a driveBase object
// ********************************************************************** //

#ifndef motorController_h
#define motorController_h

// Include necessary libraries
#include "motorCommands.h"
#include "options.h"

class motorController {
    public:
        // Constructors
        static motorController* getInstance();

        // Functions
        void update();
        
    private:
        // Constructors
        motorController();

        // Variables
        static bool _instanceCreated;                                   // Stores whether instance has been created
        static motorController* _instance;                              // Stores instance
        static uint8_t _address;                                        // Stores address for I2C

        static uint16_t _supplyVoltage;                                 // Stores the supply voltage (in milliVolts)
        static uint16_t _temperature;                                   // Stores the temperature
        static motorCommand _motorCommand;                              // Stores which variable to be sent
        static uint8_t _motorSpeed;                                     // Stores the output motor speed
        static bool _motorDirection;                                    // Stores the output motor direction
        static bool _motorSpeedOrientation;                             // Stores the motor speed orientation
        static bool _motorDirectionOrientation;                         // Stores the motor direction orientation
        static bool _motorPowerOutput;                                  // Stores the motor power output
        static uint8_t _config;                                         // Stores the motor configuration

        // Getters
        static uint8_t _getAddress();
        static int32_t _getEncoderPosition();
        static uint16_t _getEncoderSpeed();
        static uint16_t _getSupplyVoltage();
        static uint16_t _getTemperature();
        static motorCommand _getMotorCommand();
        static uint8_t _getMotorSpeed();
        static bool _getMotorDirection();
        static bool _getMotorSpeedOrientation();
        static bool _getMotorDirectionOrientation();
        static uint16_t _getMotorVoltage();
        static uint16_t _getMotorCurrent();
        static uint16_t _getMotorPower();
        static bool _getMotorPowerOutput();
        static bool _getConfig();

        // Setters
        static void _setAddress(uint8_t desiredAddress);
        static void _setEncoderPosition(int32_t desiredPosition);
        static void _setEncoderPositionZero();
        static void _setEncoderSpeedPrescaler(uint8_t desiredEncoderSpeedPrescaler);
        static void _setSupplyVoltage(uint16_t desiredSupplyVoltage);
        static void _setMotorCommand(motorCommand desiredMotorCommand);
        static void _setMotorSpeed(uint8_t desiredMotorSpeed);
        static void _setMotorDirection(bool desiredMotorDirection);
        static void _setMotorSpeedOrientation(bool desiredMotorSpeedOrientation);
        static void _setMotorDirectionOrientation(bool desiredMotorDirectionOrientation);
        static void _setMotorPowerOutput(bool desiredMotorPowerOutput);
        static void _setConfig(uint8_t desiredConfig);

        // Functions
        static void _sendVariableUnsignedTwoBytes(uint16_t variableToSend);
        static void _sendVariableSignedFourBytes(int32_t variableToSend);
        static void _sendVariableUnsignedFourBytes(uint32_t variableToSend);
        static uint8_t _receiveVariableUnsignedOneByte();
        static int32_t _receiveVariableSignedFourBytes();
        static void _requestEvent();
        static void _receiveEvent(int numberBytes);

};

#endif