// ********************************************************************** //
// Nathaniel Belles
// driveBase - encoder.cpp
// Purpose: Creates an object of type 'encoder' to use to interface with
//          a single encoder controller. Handles all I2C communication 
//          required. 
// ********************************************************************** //

// Include necessary libraries
#include "motor.h"
#include "motorConverters.h"
#include <Wire.h>

S32toU8 converterS32toU8;
U32toU8 converterU32toU8;
U16toU8 converterU16toU8;

//******************************************************************************//
// Constructors
//******************************************************************************//

// Setup motor instance with provided address
motor::motor(uint8_t address) {
    // Store controller address
    _address = address;

    // Initiate Wire library with address
    Wire.begin();
        
    // Adjust clock
    // Wire.setClock(400000);
}

//******************************************************************************//
// Public Getters
//******************************************************************************//

// Request current position
int32_t motor::getEncoderPosition() {
    // Send command to request variable
    _sendMotorCommand(GetEncoderPosition);
    
    // Read response
    return _getSignedFourBytes();
}

// Request current speed
float motor::getEncoderSpeed() {
    // Send command to request variable
    _sendMotorCommand(GetEncoderSpeed);

    // Store speed unscaled
    uint32_t encoderSpeed = 20000000;

//    return (_getConfig() & B01110000) >> 4;

    // Scale by speed prescaler
    switch((_getConfig() & B01110000) >> 4) {
      case B000 : 
        encoderSpeed = encoderSpeed >> 0; // /1
        break;
      case B001 : 
        encoderSpeed = encoderSpeed >> 1; // /2
        break;
      case B010 : 
        encoderSpeed = encoderSpeed >> 2; // /4
        break;
      case B011 : 
        encoderSpeed = encoderSpeed >> 3; // /8
        break;
      case B100 :
        encoderSpeed = encoderSpeed >> 4; // /16
        break;
      case B101 :
        encoderSpeed = encoderSpeed >> 6; // /64
        break;
      case B110 :
        encoderSpeed = encoderSpeed >> 8; // /256
        break;
      case B111 :
        encoderSpeed = encoderSpeed >> 10; // /1024
        break;
      default : 
        // Do nothing
        break;
    }

    // Return final value
    return (float)encoderSpeed / _getUnsignedTwoBytes();
}

// Request motor voltage
uint16_t motor::getMotorVoltage() {
    // Send command to request variable
    _sendMotorCommand(GetMotorVoltage);

    // Read response
    return _getUnsignedTwoBytes();
}

// Request motor current
uint16_t motor::getMotorCurrent() {
    // Send command to request variable
    _sendMotorCommand(GetMotorCurrent);

    // Read response
    return _getUnsignedTwoBytes();
}

// Request motor power
uint16_t motor::getMotorPower() {
    // Send command to request variable
    _sendMotorCommand(GetMotorPower);

    // Read response
    return _getUnsignedTwoBytes();
}

// Request supply voltage
uint16_t motor::getSupplyVoltage() {
    // Send command to request variable
    _sendMotorCommand(GetSupplyVoltage);

    // Read response
    return _getUnsignedTwoBytes();
}

// Request temperature
uint16_t motor::getTemperature() {
    // Send command to request variable
    _sendMotorCommand(GetTemperature);

    // Read response
    return _getUnsignedTwoBytes();
}

// Get status
uint8_t motor::getStatus() {
    // Return status
    return _status;
}

//******************************************************************************//
// Public Setters
//******************************************************************************//

// Set encoder position to desired value
void motor::setEncoderPosition(int32_t desiredEncoderPosition) {
    // Constrain input
    desiredEncoderPosition = constrain(desiredEncoderPosition, -2147483648, 2147483648);

    // Send command
    _sendMotorCommand(SetEncoderPosition);

    // Send variable
    _putSignedFourBytes(desiredEncoderPosition);
}

// Set motor position to zero
void motor::setEncoderPositionZero() {
    // Send command
    _sendMotorCommand(SetEncoderPositionZero);
}

// Set motor power output
void motor::setEncoderSpeedPrescaler(uint8_t desiredEncoderSpeedPrescaler) {
    // Constrain input
    desiredEncoderSpeedPrescaler = constrain(desiredEncoderSpeedPrescaler, 0, 7);

    // Store prescaler in config
    _setConfig((_getConfig() & B10001111) | (desiredEncoderSpeedPrescaler << 4));

    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(_getConfig());
}

// Set motor speed
void motor::setMotorSpeed(uint8_t desiredMotorSpeed) {
    // Constrain input
    desiredMotorSpeed = constrain(desiredMotorSpeed, 0, 255);

    // Send command
    _sendMotorCommand(SetMotorSpeed);

    // Send variable
    _putUnsignedOneByte(desiredMotorSpeed);
}

// Set motor direction
void motor::setMotorDirection(bool desiredMotorDirection) {
    // Constrain input
    desiredMotorDirection = constrain(desiredMotorDirection, 0, 1);

    // Store desired motor direction in config
    if (desiredMotorDirection) {
        _setConfig(_getConfig() | B00000001);
    } 
    else {
        _setConfig(_getConfig() & B11111110);
    }
    
    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(_getConfig());
}

// Set motor speed orientation
void motor::setMotorSpeedOrientation(bool desiredMotorSpeedOrientation) {
    // Constrain input
    desiredMotorSpeedOrientation = constrain(desiredMotorSpeedOrientation, 0, 1);

    // Store motor speed orientation in config
    if (desiredMotorSpeedOrientation) {
        _setConfig(_getConfig() | B00000010);
    } 
    else {
        _setConfig(_getConfig() & B11111101);
    }

    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(_getConfig());
}

// Set motor direction orientation
void motor::setMotorDirectionOrientation(bool desiredMotorDirectionOrientation) {
    // Constrain input
    desiredMotorDirectionOrientation = constrain(desiredMotorDirectionOrientation, 0, 1);

    // Store motor direction orientation in config
    if (desiredMotorDirectionOrientation) {
        _setConfig(_getConfig() | B00000100);
    } 
    else {
        _setConfig(_getConfig() & B11111011);
    }

    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(_getConfig());
}

// Set motor power output
void motor::setMotorPowerOutput(bool desiredMotorPowerOutput) {
    // Constrain input
    desiredMotorPowerOutput = constrain(desiredMotorPowerOutput, 0, 1);

    // Store motor power output in config
    if (desiredMotorPowerOutput) {
        _setConfig(_getConfig() | B00001000);
    } 
    else {
        _setConfig(_getConfig() & B11110111);
    }

    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(_getConfig());
}

// Set motor config
void motor::setConfig(uint8_t desiredConfig) {
    // Constrain input
    desiredConfig = constrain(desiredConfig, 0, 255);

    // Store config
    _setConfig(desiredConfig);

    // Send command
    _sendMotorCommand(SetConfig);

    // Send variable
    _putUnsignedOneByte(desiredConfig);
}

//******************************************************************************//
// Private Getters
//******************************************************************************//

// Get config
uint8_t motor::_getConfig() {
    // Return variable
    return _config;
}

//******************************************************************************//
// Private Setters
//******************************************************************************//

// Set config
void motor::_setConfig(uint8_t desiredConfig) {
    // Store variable
    _config = desiredConfig;
}

//******************************************************************************//
// Private Functions
//******************************************************************************//

// Send command as byte 
void motor::_sendMotorCommand(motorCommand commandToSend) {
    // Begin transmission
    Wire.beginTransmission(_address);
    
    // Write command
    Wire.write(commandToSend);

    switch (commandToSend) {
      case GetEncoderPosition :
      case GetEncoderSpeed :
      case GetSupplyVoltage :
        // Finish sending command so data can be received and store returned value as status
        _status = Wire.endTransmission();
        break;
      default : 
        // Do nothing
        break;
    }
}

// Receives one unsigned byte from address
uint8_t motor::_getUnsignedOneByte() {
    // Clear Wire bus
    if (Wire.available() > 0) {
        _clearWire();
    }

    // Request one unsigned byte from address
    Wire.requestFrom(_address, 1);
    
    // Store one unsigned byte
    uint8_t variableToReceive = Wire.read();

    // Return variableToReceive
    return variableToReceive;
}

// Receives two bytes from address
uint16_t motor::_getUnsignedTwoBytes() {
    // Clear Wire bus
    if (Wire.available() > 0) {
        _clearWire();
    }
    
    // Initialize variable received
    uint16_t variableToReceive;

    // Request two bytes from address
    Wire.requestFrom(_address, 2);

    // Cycle through each byte of variable received
    for (int i = 0; i < sizeof(variableToReceive); i++) {
        // Read each byte from Wire bus
        converterU16toU8.conversionU8[i] = Wire.read();
    }

    // Store final variable from conversion
    variableToReceive = converterU16toU8.conversionU16;

    // Return final variable
    return variableToReceive;
}

// Request four bytes from address
int32_t motor::_getSignedFourBytes() {
    // Clear Wire bus
    if (Wire.available() > 0) {
        _clearWire();
    }
    
    // Initialize variable received
    int32_t variableToReceive;

    // Request four bytes from address
    Wire.requestFrom(_address, 4);

    // Cycle through each byte of variable received
    for (int i = 0; i < sizeof(variableToReceive); i++) {
        // Read each byte from Wire bus
        converterS32toU8.conversionU8[i] = Wire.read();
    }

    // Store final variable from conversion
    variableToReceive = converterS32toU8.conversionS32;

    // Return final variable
    return variableToReceive;
}

// Receives four bytes from address
uint32_t motor::_getUnsignedFourBytes() {
    // Clear Wire bus
    if (Wire.available() > 0) {
        _clearWire();
    }
    
    // Initialize variable received
    uint32_t variableToReceive;

    // Request four bytes from address
    Wire.requestFrom(_address, 4);

    // Cycle through each byte of variable received
    for (int i = 0; i < sizeof(variableToReceive); i++) {
        // Read each byte from Wire bus
        converterU32toU8.conversionU8[i] = Wire.read();
    }

    // Store final variable from conversion
    variableToReceive = converterU32toU8.conversionU32;

    // Return final variable
    return variableToReceive;
}

// Send one byte
void motor::_putUnsignedOneByte(uint8_t variableToSend) {
    // Write byte
    Wire.write(variableToSend);

    // End transmission
    _status = Wire.endTransmission();
}

// Send multiple bytes
void motor::_putSignedFourBytes(int32_t variableToSend) {
    // Convert variable to bytes
    converterS32toU8.conversionS32 = variableToSend;

    // Cycle through each byte of variable received
    for (int i = 0; i < sizeof(variableToSend); i++) {
        // Write each byte to Wire bus
        Wire.write(converterS32toU8.conversionU8[i]);
    }

    // End transmission
    _status = Wire.endTransmission();
}

// Clear all available data from Wire bus
void motor::_clearWire() {
    while (Wire.available() > 0) {
        Wire.read();
    }
}