// ********************************************************************** //
// Nathaniel Belles
// driveBase - motorCommands.h
// Purpose: Creates enum with all the necessary commands to communicate 
//          with a motor controller
// ********************************************************************** //

#ifndef motorCommands_H
#define motorCommands_H

// Create enumeration
enum motorCommand {
    DefaultValue,
    GetEncoderPosition,
    GetEncoderSpeed,
    GetMotorVoltage,
    GetMotorCurrent,
    GetMotorPower,
    GetSupplyVoltage,
    GetTemperature,
    SetEncoderPosition,
    SetEncoderPositionZero,
    SetMotorSpeed,
    SetMotorDirection,
    SetConfig
};

#endif