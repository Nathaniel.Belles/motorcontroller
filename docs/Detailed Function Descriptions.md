## Constructors
### motor()
#### Description
This should be called in the beginning of a program to initialize a motor object. This can then be used to read and write data from the motorController. Multiple instances of motor() can be created to communicate multiple motorControllers all connected on the same I2C bus. 
#### Inputs
`uint8_t desiredAddress` : This is the I2C address of the motorController and is stored for all future communication with this motorController.
#### Outputs
`motor` : Creates an instance of the object of type motor that can be used to communicate with a motorController. 
#### Example
```
motor motor1(B00000001);
```
## Getters
### getEncoderPosition()
#### Description
When called on a motorController object, this requests the current position of the encoder from the motorController and returns it. 
#### Inputs
None.
#### Outputs
`int32_t` : Returns the current position of the encoder as a signed four byte integer. Range: -2,147,483,648 to 2,147,483,648
#### Example
```
int32_t currentPosition = motor1.getEncoderPosition(); 
``` 
### getEncoderSpeed()
#### Description
When called on a motorController object, this requests the current speed of the encoder from the motorController and returns it. Every 1/256th of a second the number of encoder ticks generated is stored as the speed. This value can be multiplied by (256 * Pulses Per Revolution) to get the speed of the motor in revolutions per minute. This is currently under development to find a better way to calculate overall speed of the encoder with less jitter. I'm all ears for suggestions. 
#### Inputs
None.
#### Outputs
`int32_t` : Returns the current speed of the encoder as a signed four byte integer in encoder ticks per 1/256th of a second. Range: -2,147,483,648 to 2,147,483,648
#### Example
```
int32_t encoderRPM = motor1.getEncoderSpeed() * 256 * ENCODER_PPR;
```
### getMotorVoltage()
#### Description
When called on a motorController object, this requests the motor voltage from the motorController and returns it. The value can be "calibrated" by editing the #define for upper and lower bound of the voltage reading in the options.h file before programming the motorController. 
#### Inputs
None.
#### Outputs
`uint16_t` : Returns the motor voltage as an unsigned two byte integer in milliVolts. Range: 0 to 65,536
#### Example
```
uint16_t motorVoltage = motor1.getMotorVoltage();
```
### getMotorCurrent()
#### Description
When called on a motorController object, this requests the motor current from the motorController and returns it. The value can be "calibrated" by editing the #define for upper and lower bound of the current reading in the options.h file before programming the motorController. 
#### Inputs
None.
#### Outputs
`uint16_t` : Returns the motor current as an unsigned two byte integer in milliAmps. Range 0 to 65,536
#### Example
```
uint16_t motorCurrent = motor1.getMotorCurrent();
```
### getMotorPower()
#### Description
When called on a motorController object, this requests the motor power from the motorController and returns it. The value can be "calibrated" by editing the #define for upper and lower bound of the voltage and current readings in the options.h file before programming the motorController. 
#### Inputs
None.
#### Outputs
`uint16_t` : Returns the motor power as an unsigned two byte integer in milliWatts. Range: 0 to 65,536
#### Example
### getSupplyVoltage()
#### Description
When called on a motorController object, this requests the supply voltage of the IC from the motorController and returns it. This value is currently has an error of +/- 100mV but will hopefully be improved in the future.
#### Inputs
None.
#### Outputs
`uint16_t` : Returns the supply voltage of the motorController IC as an unsigned two byte integer in milliVolts. Range: 0 to 65,536
#### Example
```
uint16_t supplyVoltage = motor1.getSupplyVoltage();
```
### getTemperature()
#### Description
When called on a motorController object, this requests the temperature of the IC from the motorController and returns it. This value is currently has a lot of error but will hopefully be improved in the future. This is not meant for determining the actual temperature of the die but is an approximation for determining if something is wrong or overheating.
#### Inputs
None.
#### Outputs
`uint16_t` : Returns the temperature of the motorController IC as an unsigned two byte integer in degrees celsius. Range: 0 to 65,536
#### Example
```
uint16_t temperature = motor1.getTemperature();
```
## Setters

### setEncoderPosition()
#### Description
When called on a motorController object, this sets the position of the encoder on the  motorController. If intent is to set to zero, it is faster to use `setEncoderPositionZero()`.
#### Inputs
`int32_t desiredEncoderPosition` : Sets the encoder position of the motorController encoder to the specified position. Range: -2,147,483,648 to 2,147,483,648
#### Outputs
None.
#### Example
```
motor1.setEncoderPosition(123456);
```
### setEncoderPositionZero()
#### Description
When called on a motorController object, this sets the position of the encoder on the  motorController to zero. This is faster than `setEncoderPosition()` if setting the value to zero.
#### Inputs
None.
#### Outputs
None.
#### Example
```
motor1.setEncoderPositionZero();
```
### setMotorSpeed()
#### Description
When called on a motorController object, this sets the 8-bit PWM duty cycle output on the motorController to the value specified. This is used for controlling the speed of a motor. This is not a high current output and should be sent through a motor driver before conecting to the motor. The fequency of the output is approximately 1.25kHz. 
#### Inputs
`uint8_t desiredMotorSpeed` : Sets the motor speed output duty-cycle on the motorController. Range: 0 to 255
#### Outputs
None.
#### Example
```
motor1.setMotorSpeed(127);
```
### setMotorDirection()
#### Description
When called on a motorController object, this sets the binary output on the motorController to the value specified. This is used for controlling the direction of a motor. 0 is used for normal operation and 1 is used for inverted operation. This configuration will be saved to the EEPROM every time it is called and will be used the next time it is turned on.
#### Inputs
`bool desiredMotorDirection` : Sets the motor direction output on the motorController. Range: 0 or 1
#### Outputs
None.
#### Example
```
motor1.setMotorDirection(1);
```
### setMotorSpeedOrientation()
#### Description
When called on a motorController object, this sets the motor speed orientation on the motorController to the value specified. This can be used for inverting the PWM duty cycle if a motor has a non-normal configuration. 0 is used to set the motor speed to normal (0 means off and 255 means fully on). 1 is used to set the motor speed to inverted (0 means fully on and 255 means off). This configuration will be saved to the EEPROM every time it is called and will be used the next time it is turned on.
#### Inputs
`bool desiredMotorSpeedOrientation` : Sets the motor speed orientation on the motorController. 0 is used to set the motor speed to normal (0 means off and 255 means fully on). 1 is used to set the motor speed to inverted (0 means fully on and 255 means off). Range: 0 or 1
#### Outputs
None.
#### Example
```
motor1.setMotorSpeedOrientation(1);
```
### setMotorDirectionOrientation()
#### Description
When called on a motorController object, this sets the motor direction orientation on the motorController to the value specified. This can be used for inverting the motor direction output if a motor has a non-normal configuration. 0 is used to set the motor speed to normal (0 means off and 255 means fully on). 1 is used to set the motor speed to inverted (0 means fully on and 255 means off). This configuration will be saved to the EEPROM every time it is called and will be used the next time it is turned on.
#### Inputs
`bool desiredMotorDirectionOrientation` : Sets the motor direction orientation on the motorController. 0 is used to set the motor direction to normal (0 means motorDirection = motorDirection). 1 is used to set the motor direction to inverted (0 means motorDirection = !motorDirection). Range: 0 or 1
#### Outputs
None.
#### Example
```
motor1.setMotorDirectionOrientation(1);
```
### setMotorConfig()
#### Description
When called on a motorController object, this sets the motor configuration on the motorController to the value specified. This is useful for making multiple changes to the motor configuration at once and is **significantly** faster than setting each of the motor configurations individually. This configuration will be saved to the EEPROM every time it is called and will be used the next time it is turned on.

| Motor Direction Orientation | Motor Speed Orientation | Motor Direction | Configuration Byte |
| :-------------------------: | :---------------------: | :-------------: | :----------------: |
|           Normal            |         Normal          |     Forward     |    `B00000000`     |
|           Normal            |         Normal          |     Reverse     |    `B00000001`     |
|           Normal            |        Inverted         |     Forward     |    `B00000010`     |
|           Normal            |        Inverted         |     Reverse     |    `B00000011`     |
|          Inverted           |         Normal          |     Forward     |    `B00000100`     |
|          Inverted           |         Normal          |     Reverse     |    `B00000101`     |
|          Inverted           |        Inverted         |     Forward     |    `B00000110`     |
|          Inverted           |        Inverted         |     Reverse     |    `B00000111`     |
#### Inputs
`uint8_t desiredMotorConfig` : Sets the motor configuration according to the table above. Range: 0 to 7
#### Outputs
None.
#### Example
```
motor1.setMotorConfig(B00000101);
```